#ifndef RAY_H
#define RAY_H

#include "vec3.h"

// class for a ray
class ray {
public:
    ray() {}
    ray(const point3& origin, const vec3& direction)
            : orig(origin), dir(direction)
    {}

    point3 origin() const  { return orig; }
    vec3 direction() const { return dir; }

    /*
     * Purpose: calculate a point on the ray at time t
     * Inputs: time t
     * Return value(s): coordinates of the point
     */
    point3 at(double t) const {
        return orig + t * dir;
    }

public:
    point3 orig;
    vec3 dir;
};

#endif