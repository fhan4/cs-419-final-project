#ifndef BVH_H
#define BVH_H

#include "rtweekend.h"

#include "hittable.h"
#include "hittable_list.h"

#include <algorithm>

class bvh_node : public hittable {
public:
    bvh_node();

    bvh_node(const hittable_list& list, double time0, double time1)
            : bvh_node(list.objects, 0, list.objects.size(), time0, time1, 0)
    {}
    bvh_node(const shared_ptr<hittable_list>& list, double t0=0.0, double t1=const_inf) 
            : bvh_node(list->objects, 0, list->objects.size(), t0, t1, 0) {}

    bvh_node(
            const std::vector<shared_ptr<hittable>>& src_objects,
            size_t start, size_t end, double time0, double time1, int depth);

    virtual bool hit(
            const ray& r, double t_min, double t_max, hit_record& rec) const override;

    virtual bool bounding_box(double time0, double time1, aabb& output_box) const override;

public:
    shared_ptr<hittable> left;
    shared_ptr<hittable> right;
    aabb box;
};

/*
 * Purpose: compares two bounding boxes for a given axis
 * Inputs: pointers to bounding boxes a and b, and the axis they are compared on given as an int
 * Return value(s): boolean value showing which bounding box starts earlier
 */
inline bool box_compare(const shared_ptr<hittable> a, const shared_ptr<hittable> b, int axis) {
    aabb box_a;
    aabb box_b;

    if (!a->bounding_box(0,0, box_a) || !b->bounding_box(0,0, box_b))
        std::cerr << "No bounding box in bvh_node constructor.\n";

    return box_a.min().e[axis] < box_b.min().e[axis];
}

/*
 * Purpose: compares two bounding boxes on the x axis
 * Inputs: pointers to bounding boxes a and b
 * Return value(s): boolean value showing which bounding box starts earlier
 */
bool box_x_compare (const shared_ptr<hittable> a, const shared_ptr<hittable> b) {
    return box_compare(a, b, 0);
}

/*
 * Purpose: compares two bounding boxes on the y axis
 * Inputs: pointers to bounding boxes a and b
 * Return value(s): boolean value showing which bounding box starts earlier
 */
bool box_y_compare (const shared_ptr<hittable> a, const shared_ptr<hittable> b) {
    return box_compare(a, b, 1);
}

/*
 * Purpose: compares two bounding boxes on the z axis
 * Inputs: pointers to bounding boxes a and b
 * Return value(s): boolean value showing which bounding box starts earlier
 */
bool box_z_compare (const shared_ptr<hittable> a, const shared_ptr<hittable> b) {
    return box_compare(a, b, 2);
}

/*
 * Purpose: Constructor. Take a list of hittables and generate a BVH that stores all hittables as leaf nodes and create
 *          non-leaf nodes to contain surrounding boxes of their child nodes. Each node is also a hittable that is used
 *          to quickly filter out rays that certainly will not hit any of its child nodes.
 * Inputs: a list of hittables src_objects, size of the list start and end, time range time0 and time1
 */
bvh_node::bvh_node(
        const std::vector<shared_ptr<hittable>>& src_objects,
        size_t start, size_t end, double time0, double time1, int depth
) {
    auto objects = src_objects;

    int axis = 0;
    double max_range = 0;
    double mid_point = 0;

    for (int i = 0; i < 3; i++) {
        double min_center = std::numeric_limits<double>::max();
        double max_center = -std::numeric_limits<double>::max();
        for (int j = start; j < end; j++) {
            aabb temp_box;
            if (objects[j]->bounding_box(time0, time1, temp_box)) {
                if (temp_box.cen().e[i] < min_center) { min_center = temp_box.cen().e[i]; }
                if (temp_box.cen().e[i] > max_center) { max_center = temp_box.cen().e[i]; }
            }
        }

        if (max_center - min_center > max_range) {
            max_range = max_center - min_center;
            mid_point = (max_center + min_center) / 2;
            axis = i;
        }
    }

    size_t object_span = end - start;

    if (object_span == 1) {
        left = right = objects[start];
    } else if (object_span == 2) {
        left = objects[start];
        right = objects[start+1];
    } else {
        auto iter = std::partition(objects.begin() + start, objects.begin() + end,
                                 [axis, mid_point](const shared_ptr<hittable> a) {
                                     aabb box_a;
                                     if (a->bounding_box(0, 1, box_a)) {
                                         return box_a.cen().e[axis] < mid_point;
                                     }
                                 });

        auto mid = iter - objects.begin();

        left = make_shared<bvh_node>(objects, start, mid, time0, time1, depth + 1);
        right = make_shared<bvh_node>(objects, mid, end, time0, time1, depth + 1);
    }

    aabb box_left, box_right;

    if (  !left->bounding_box (time0, time1, box_left)
          || !right->bounding_box(time0, time1, box_right)
            )
        std::cerr << "No bounding box in bvh_node constructor.\n";

    box = surrounding_box(box_left, box_right);
}

/*
 * Purpose: bounding box function for the node
 * Inputs: range time0 and time1, pointer to aabb output_box
 * Return value(s): boolean value true, bounding box of the node is stored in output_box
 */
bool bvh_node::bounding_box(double time0, double time1, aabb& output_box) const {
    output_box = box;
    return true;
}

/*
 * Purpose: hit function for the bvh node, traverse the BVH to figure out whether anything is hit, optimize ray tracing
 *          by returning false for rays that never hit the box without checking its children
 * Inputs: ray r, range t_min and t_max, hit record rec
 * Return value(s): boolean value showing whether the ray hits anything under the node
 */
bool bvh_node::hit(const ray& r, double t_min, double t_max, hit_record& rec) const {
    if (!box.hit(r, t_min, t_max))
        return false;

    bool hit_left = left->hit(r, t_min, t_max, rec);
    bool hit_right = right->hit(r, t_min, hit_left ? rec.t : t_max, rec);

    return hit_left || hit_right;
}

#endif