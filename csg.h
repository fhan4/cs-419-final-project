#ifndef CSG_H
#define CSG_H

#include <cmath>
#include <tuple>
#include <vector>
#include <utility>
#include <cassert>
#include <algorithm>
#include "vec3.h"
#include "rtweekend.h"
#include "hittable.h"
#include "material.h"

class csg_parent : public hittable {
	public:
		/** Function: csg_parent(...) (Constructor)
			Description: Creates a csg primitive where the node does an op on the two objects it points to
			Inputs:  
				obj_a, obj_b (type:hittable*) - two objects to perform the op
				mat_override (type:material*) - material point override (optional)
			Outputs: NA
		**/
		csg_parent() {}
		csg_parent(shared_ptr<hittable> obj_a, shared_ptr<hittable> obj_b) : left(obj_a), right(obj_b), mat_ovr(nullptr) {}
		csg_parent(shared_ptr<hittable> obj_a, shared_ptr<hittable> obj_b, shared_ptr<material> mat_override) : left(obj_a), right(obj_b), mat_ovr(mat_override) {}

		/** Function: bounding_box(...)
	        Description: Inherited bounding_box function from parent. Creates a bounding box for the geometry primitive
	        Inputs: 
	            out_box (type:aabb)    - created axis aligned bounding box that is passed to caller as reference.
	        Outputs:
	            bool    (type:boolean) - returns true if aabb exists.
	    **/
		virtual bool bounding_box(double t0, double t1, aabb& out_box) const override;

	public:
		shared_ptr<hittable> left, right;
		shared_ptr<material> mat_ovr;
};

class csg_union : public csg_parent {
	public:
		using csg_parent::csg_parent; // inherits the constructor from the parent class

		/** Function: hit(...)
			Description: Inherited hit function from parent. Checks if the ray has hit the object
			Inputs: 
				r 	  (type:ray) 	  	- casted ray that is checked against the geometry primitive
				t_min (type:double) 	- time minimum
				t_max (type:double) 	- time maximum
				rec   (type:hit_record) - record struct used to keep track of hit information if a hit is detected
			Outputs:
				bool  (type:boolean)	- returns if it hit something or not.
		**/
		virtual bool hit(const ray& r, double t_min, double t_max, hit_record& rec) const override;
};

class csg_intersection : public csg_parent {
	public:
		using csg_parent::csg_parent; // inherits the constructor from the parent class

		/** Function: hit(...)
			Description: Inherited hit function from parent. Checks if the ray has hit the object
			Inputs: 
				r 	  (type:ray) 	  	- casted ray that is checked against the geometry primitive
				t_min (type:double) 	- time minimum
				t_max (type:double) 	- time maximum
				rec   (type:hit_record) - record struct used to keep track of hit information if a hit is detected
			Outputs:
				bool  (type:boolean)	- returns if it hit something or not.
		**/
		virtual bool hit(const ray& r, double t_min, double t_max, hit_record& rec) const override;
};

class csg_difference : public csg_parent {
	public:
		using csg_parent::csg_parent; // inherits the constructor from the parent class

		/** Function: hit(...)
			Description: Inherited hit function from parent. Checks if the ray has hit the object
			Inputs: 
				r 	  (type:ray) 	  	- casted ray that is checked against the geometry primitive
				t_min (type:double) 	- time minimum
				t_max (type:double) 	- time maximum
				rec   (type:hit_record) - record struct used to keep track of hit information if a hit is detected
			Outputs:
				bool  (type:boolean)	- returns if it hit something or not.
		**/
		virtual bool hit(const ray& r, double t_min, double t_max, hit_record& rec) const override;
};

/* -------------------------------------------------------------------------------------------------------------------------------- */

/** Function: bbox(...)
        Description: Inherited bbox function from parent. Creates a bounding box for the geometry primitive
        Inputs: 
            out_box (type:aabb)    - created axis aligned bounding box that is passed to caller as reference.
        Outputs:
            bool    (type:boolean) - returns true if aabb exists.
    **/
bool csg_parent::bounding_box(double t0, double t1, aabb& out_box) const {
	aabb box_left, box_right;
	bool lbbox_exists = left->bounding_box(t0, t1, box_left);
	bool rbbox_exists = right->bounding_box(t0, t1, box_right);
	if (!rbbox_exists && !lbbox_exists) {return false;}
	else if (!rbbox_exists && lbbox_exists) {out_box = box_left; return true;} 
	else if (rbbox_exists && !lbbox_exists) {out_box = box_right; return true;} 
	aabb csg_box = surrounding_box(box_left, box_right); 
	out_box = csg_box;
	return true;
}

/** Function: hit_record_t_sort(...)
	Description: sort function for sorting a vector of hittables - sorts according to t value
**/
bool hit_record_t_sort(const hit_record& a, const hit_record& b) {return (a.t < b.t);}
bool hit_record_t_sort_pair(const std::pair<hit_record, bool>& a, const std::pair<hit_record, bool>& b) {return (a.first.t < b.first.t);}

/** Function: *csg_type*::hit(...)
	Description: Inherited hit function from parent. Checks if the ray has hit the object given the csg type
	Inputs: 
		r 	  (type:ray) 	  	- casted ray that is checked against the geometry primitive
		t_min (type:double) 	- time minimum
		t_max (type:double) 	- time maximum
		rec   (type:hit_record) - record struct used to keep track of hit information if a hit is detected
	Outputs:
		bool  (type:boolean)	- returns if it hit something or not.
**/
bool csg_union::hit(const ray& r, double t_min, double t_max, hit_record& rec) const {
	// first check if any of the objects have been hit
	std::vector<std::tuple<hit_record, bool, bool>> record_vector(4);
	std::get<1>(record_vector[0]) = left->hit(r, -const_inf, const_inf, std::get<0>(record_vector[0]));
	std::get<1>(record_vector[1]) = left->hit(r, std::get<0>(record_vector[0]).t+0.0001, const_inf, std::get<0>(record_vector[1]));
	std::get<1>(record_vector[2]) = right->hit(r, -const_inf, const_inf, std::get<0>(record_vector[2]));
	std::get<1>(record_vector[3]) = right->hit(r, std::get<0>(record_vector[2]).t+0.0001, const_inf, std::get<0>(record_vector[3]));
	std::get<2>(record_vector[0]) = 0;
	std::get<2>(record_vector[1]) = 0;
	std::get<2>(record_vector[2]) = 1;
	std::get<2>(record_vector[3]) = 1;

	// filter hits
	std::vector<std::pair<hit_record, bool>> filtered_hits;
	for (auto& c : record_vector) {if (std::get<1>(c)) {filtered_hits.push_back(std::pair<hit_record, bool>(std::get<0>(c), std::get<2>(c)));}}
	if (filtered_hits.empty()) return false;
	std::sort(filtered_hits.begin(), filtered_hits.end(), hit_record_t_sort_pair);

	// detect any repeats in the filtered, sorted sequence
	bool repeat_detected = false;
	int repeat_idx = -1; 
	for (size_t i = 1; i < filtered_hits.size(); i++) {
		if (filtered_hits[i-1].second == filtered_hits[i].second) {
			repeat_detected = true;
			repeat_idx = i-1;
			break;
	}}

	// if the sequence of hits of the two primitives is repeated (or pairwise) - we have to go through each element of the filtered hits to find the closest hit
	if (repeat_detected && repeat_idx%2 == 0) {
		for (auto& elem : filtered_hits) {
			if (t_min < elem.first.t && elem.first.t < t_max) {
				rec = elem.first;
				if (mat_ovr != nullptr) rec.mat_ptr = mat_ovr;
				return true;
	}}}
	// else we only check the front and back
	else {
		const hit_record& closest_hit = filtered_hits.front().first;
		const hit_record& farthest_hit = filtered_hits.back().first;
		if (t_min < closest_hit.t && closest_hit.t < t_max) {
			rec = closest_hit;
			if (mat_ovr != nullptr) rec.mat_ptr = mat_ovr;
			return true;
		}
		if (t_min < farthest_hit.t && farthest_hit.t < t_max) {
			rec = farthest_hit;
			if (mat_ovr != nullptr) rec.mat_ptr = mat_ovr;
			return true;
	}}

	return false;
}
bool csg_intersection::hit(const ray& r, double t_min, double t_max, hit_record& rec) const {
	// first check if any of the objects have been hit
	std::vector<std::tuple<hit_record, bool, bool>> record_vector(4);
	std::get<1>(record_vector[0]) = left->hit(r, -const_inf, const_inf, std::get<0>(record_vector[0]));
	std::get<1>(record_vector[1]) = left->hit(r, std::get<0>(record_vector[0]).t+0.0001, const_inf, std::get<0>(record_vector[1]));
	std::get<1>(record_vector[2]) = right->hit(r, -const_inf, const_inf, std::get<0>(record_vector[2]));
	std::get<1>(record_vector[3]) = right->hit(r, std::get<0>(record_vector[2]).t+0.0001, const_inf, std::get<0>(record_vector[3]));
	std::get<2>(record_vector[0]) = 0;
	std::get<2>(record_vector[1]) = 0;
	std::get<2>(record_vector[2]) = 1;
	std::get<2>(record_vector[3]) = 1;

	// filter hits
	std::vector<std::pair<hit_record, bool>> filtered_hits;
	for (auto& c : record_vector) {if (std::get<1>(c)) {filtered_hits.push_back(std::pair<hit_record, bool>(std::get<0>(c), std::get<2>(c)));}}
	if (filtered_hits.empty() || (filtered_hits.size() < 4)) return false;
	std::sort(filtered_hits.begin(), filtered_hits.end(), hit_record_t_sort_pair);

	if (filtered_hits[0].second == filtered_hits[1].second) return false;
	// {// assert(filtered_hits[2].second == filtered_hits[3].second);
		// return false;// }

	double near_t = filtered_hits[1].first.t;
	double far_t = filtered_hits[2].first.t;
	if (t_min < near_t && near_t < t_max) {
		rec = filtered_hits[1].first;
		if (mat_ovr != nullptr) rec.mat_ptr = mat_ovr;
		return true;
	}
	if (t_min < far_t && far_t < t_max) {
		rec = filtered_hits[2].first;
		if (mat_ovr != nullptr) rec.mat_ptr = mat_ovr;
		return true;
	}

	return false;
}
bool csg_difference::hit(const ray& r, double t_min, double t_max, hit_record& rec) const {
	// check for hits against the primitives
	std::vector<std::tuple<hit_record, bool, bool>> record_vector(4);
	std::get<1>(record_vector[0]) = left->hit(r, -const_inf, const_inf, std::get<0>(record_vector[0]));
	std::get<1>(record_vector[1]) = left->hit(r, std::get<0>(record_vector[0]).t+0.0001, const_inf, std::get<0>(record_vector[1]));
	std::get<1>(record_vector[2]) = right->hit(r, -const_inf, const_inf, std::get<0>(record_vector[2]));
	std::get<1>(record_vector[3]) = right->hit(r, std::get<0>(record_vector[2]).t+0.0001, const_inf, std::get<0>(record_vector[3]));
	std::get<2>(record_vector[0]) = 0;
	std::get<2>(record_vector[1]) = 0;
	std::get<2>(record_vector[2]) = 1;
	std::get<2>(record_vector[3]) = 1;

	// filter hits
	std::vector<std::pair<hit_record, bool>> filtered_hits;
	for (auto& c : record_vector) {
		if (std::get<1>(c)) {
			filtered_hits.push_back(std::pair<hit_record, bool>(std::get<0>(c), std::get<2>(c)));
	}}
	if (filtered_hits.empty()) return false;
	std::sort(filtered_hits.begin(), filtered_hits.end(), hit_record_t_sort_pair);

	// find where the first and second intersection points are for difference primitive
	int first_diff_hit = -1;
	bool enable_diff_check = false;
	bool mid_pair_case = false;
	for (size_t i = 0; i < filtered_hits.size(); i++) {
		if (filtered_hits[i].second) {
			if (first_diff_hit == -1) first_diff_hit = i;
			if (first_diff_hit != -1 && (first_diff_hit==1) && (i==2)) {mid_pair_case = true; break;}
			if (first_diff_hit != -1 && (i-first_diff_hit>1)) {
				// special case where the difference intersection pts surround the obj intersect pts (i.e. sequence 1001)
				if ((i-first_diff_hit) == 3) return false; 
				enable_diff_check = true;
				break;
	}}}

	// for the difference check, there are two cases, either the point is located in the first half of the hit sequence or the second half
	if (enable_diff_check || mid_pair_case) {
		if (filtered_hits.size() == 3) return false;
		// guaranteed to have 4 hits at this point - we now check 3 possible cases
		// first check the 0110 case -> if so, we need to check all of the points, else ... 
		// 		if (filtered_hits[0].second) -> hit sequence is 1010 - check hits for the second two 10[10]
		// 		if (filtered_hits[1].second) -> hit sequence is 0101 - check hits for the first two [01]01
		int start_bound = (mid_pair_case) ? 0 : ((filtered_hits[0].second) ? 2 : 0);
		int end_bound =   (mid_pair_case) ? 4 : ((filtered_hits[0].second) ? 4 : 2);
		for (int i = start_bound; i < end_bound; i++) {
			double t = filtered_hits[i].first.t;
			if (t_min < t && t < t_max) {
				rec = filtered_hits[i].first;
				if (mat_ovr != nullptr) rec.mat_ptr = mat_ovr;
				return true;
	}}}
	
	// normal hit check but we ignore the hits for the 2nd object
	else {
		for (auto& c : filtered_hits) {
			if (!c.second && (t_min < c.first.t && c.first.t < t_max)) {
				rec = c.first;
				if (mat_ovr != nullptr) rec.mat_ptr = mat_ovr;
				return true;
	}}}
	return false;
}

#endif