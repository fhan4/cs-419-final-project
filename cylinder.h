#ifndef CYLINDER_H
#define CYLINDER_H

#include <cmath>
#include "vec3.h"
#include "hittable.h"
#include "material.h"

class cylinder : public hittable {
	public:
		/** Function: cylinder(...) (Constructor)
			Description: Creates a cylinder geometry primitive
			Inputs:  
				a, b (type:vec3)      - edge points of the cylinder's axis
				r 	 (type:double)	  - radius of the cylinder
				m    (type:material*) - material pointer of the cylinder 
			Outputs: NA
		**/
		cylinder() {}
		cylinder(const point3& a, const point3& b, const double& r, const shared_ptr<material> m) : 
		_a(a), _b(b), _axis(unit_vector(a-b)), _axis_dot(dot(unit_vector(a-b), unit_vector(a-b))), radius(r), mat_ptr(m) {}
		
		/** Function: hit(...)
			Description: Inherited hit function from parent. Checks if the ray has hit the object
			Inputs: 
				r 	  (type:ray) 	  	- casted ray that is checked against the geometry primitive
				t_min (type:double) 	- time minimum
				t_max (type:double) 	- time maximum
				rec   (type:hit_record) - record struct used to keep track of hit information if a hit is detected
			Outputs:
				bool  (type:boolean)	- returns if it hit something or not.
		**/
		virtual bool hit(const ray& r, double t_min, double t_max, hit_record &hit) const override;

		/** Function: bounding_box(...)
            Description: Inherited bounding_box function from parent. Creates a bounding box for the geometry primitive
            Inputs: 
                out_box (type:aabb)    - created axis aligned bounding box that is passed to caller as reference.
            Outputs:
                bool    (type:boolean) - returns true if aabb exists.
        **/
        virtual bool bounding_box(double t0, double t1, aabb& out_box) const override;

    private:
    	bool calculate_body_hit_helper(const point3 hit_point, const double body_t, vec3& output_normal) const;

	public:
		//member variables
		point3 _a, _b;
		vec3 _axis;
		double _axis_dot;
		double radius;
		shared_ptr<material> mat_ptr;
};

bool cylinder::calculate_body_hit_helper(const point3 hit_point, const double body_t, vec3& output_normal) const {
	// Checking if the point lies within the caps (using normalized line parameterization)
	// checks if the point is on some parameter between the cap points 
	// line below does not work with fast math, it produces nans which are not guaranteed by the compiler when using fastmath optimization
	// point3 param_t = (axis_point - _b)/(_a - _b); 
	// soo... I check if the denominator is zero before dividing to prevent nans
	output_normal = unit_vector(cross(_axis, cross((hit_point-_a), _axis)));
	point3 axis_point = output_normal*-radius + hit_point;
	point3 param_denom = (_a-_b); 
	double lambda = -1.0;
	for (int i=0; i<3; i++) {if (param_denom[i] != 0.0) {lambda = (axis_point[i]-_b[i])/param_denom[i]; break;}}
	return (lambda >= 0.0 && lambda <= 1.0);
}

/** Function: hit(...)
	Description: Inherited hit function from parent. Checks if the ray has hit the object
	Inputs: 
		r 	  (type:ray) 	  	- casted ray that is checked against the geometry primitive
		t_min (type:double) 	- time minimum
		t_max (type:double) 	- time maximum
		rec   (type:hit_record) - record struct used to keep track of hit information if a hit is detected
	Outputs:
		bool  (type:boolean)	- returns if it hit something or not.
**/
bool cylinder::hit(const ray& r, double t_min, double t_max, hit_record &rec) const {
	// column intersection test
	double A = dot(r.direction(), _axis) / _axis_dot;
	double B = (dot(r.origin(), _axis) - dot(_a, _axis)) / _axis_dot;
	vec3 vB = B * _axis;
	vec3 vA = A * _axis;
	vB = r.origin() - _a - vB;
	vA = r.direction() - vA;
	A = vA.length_squared();
	B = dot(vA, vB);
	double C = vB.length_squared() - radius*radius;
	double discriminant = B*B - A*C;
	if (discriminant < 0) return false;
	double sqrt_disc = sqrt(discriminant);
	
	double temp_inv_A = 1.0/A;
	double body_t_first = (-B - sqrt_disc)*temp_inv_A;
	double body_t_second = (-B + sqrt_disc)*temp_inv_A; 

	// checking both parameter t's for the body because we need the earliest one that is valid
	vec3 first_body_normal;
	bool first_body_hit = calculate_body_hit_helper(r.at(body_t_first), body_t_first, first_body_normal);
	first_body_hit = first_body_hit && (body_t_first > t_min && body_t_first < t_max);
	vec3 second_body_normal;
	bool second_body_hit = calculate_body_hit_helper(r.at(body_t_second), body_t_second, second_body_normal);
	second_body_hit = second_body_hit && (body_t_second > t_min && body_t_second < t_max);

	bool column_hit = first_body_hit || second_body_hit;
	double body_t = 0.0;
	vec3 body_surface_normal;
	if (first_body_hit && second_body_hit) {
		body_t = 				(body_t_first < body_t_second) ? body_t_first 		: body_t_second;
		body_surface_normal = 	(body_t_first < body_t_second) ? first_body_normal 	: second_body_normal;
	} else if (first_body_hit || second_body_hit) {
		body_t = 				(first_body_hit) ? body_t_first 		: body_t_second;
		body_surface_normal = 	(first_body_hit) ? first_body_normal 	: second_body_normal;
	}

	// cap/disk intersection test
	double denom = dot(_axis, r.direction());
	bool cap_hit = false;
	double cap_t = 0.0;
	if (fabs(denom) > 1e-6) { // ray is parallel to the plane if this is false
		// check if it hits the "a" plane (dont hit if "b" is closer)
		double t1 = (dot((_a - r.origin()), _axis)/denom);
		bool hit_disc_a = (((r.at(t1) - _a).length_squared()) <= (radius*radius)) && (t1 > t_min && t1 < t_max);
		// check if it hits the "b" plane (dont hit if "a" is closer)
		double t2 = (dot((_b - r.origin()), _axis)/denom);
		bool hit_disc_b = (((r.at(t2) - _b).length_squared()) <= (radius*radius)) && (t2 > t_min && t2 < t_max);
		// get the closer valid hit point
		cap_t = (hit_disc_a) ? ((hit_disc_b) ? ((t1<t2)?t1:t2) : t1) : ((hit_disc_b)?t2:std::nan("0"));
		cap_hit = (hit_disc_a || hit_disc_b);
	}

	//calculating the closest hitpoint between the cap hits and the column hit
	// double t = (!column_hit && !cap_hit) ? std::nan("0") : ((column_hit && cap_hit)?((cap_t<body_t)?cap_t:body_t):(cap_hit?cap_t:body_t));
	// vec3 cyl_normal = (column_hit && cap_hit) ? ((cap_t<body_t) ? _axis : body_surface_normal) : (cap_hit?_axis:body_surface_normal);
	
	if (column_hit && cap_hit) {
		// test the first closest hit
		double t = (body_t < cap_t) ? body_t : cap_t;
		vec3 cyl_normal = (body_t < cap_t) ? body_surface_normal : _axis;
		if (t < t_max && t > t_min) {
			rec.t = t;
			rec.mat_ptr = mat_ptr;
			rec.set_face_normal(r, cyl_normal);
			rec.p = r.at(t);
			return true;
		}

		// test the second closest hit
		t = (body_t < cap_t) ? cap_t : body_t;
		cyl_normal = (body_t < cap_t) ? _axis : body_surface_normal;
		if (t < t_max && t > t_min) {
			rec.t = t;
			rec.mat_ptr = mat_ptr;
			rec.set_face_normal(r, cyl_normal);
			rec.p = r.at(t);
			return true;
		}
	} else if (column_hit || cap_hit) {
		double t = (column_hit) ? body_t : cap_t;
		vec3 cyl_normal = (column_hit) ? body_surface_normal : _axis;
		if (t < t_max && t > t_min) {
			rec.t = t;
			rec.mat_ptr = mat_ptr;
			rec.set_face_normal(r, cyl_normal);
			rec.p = r.at(t);
			return true;
		}
	}

	return false;
}

/** Function: bounding_box(...)
    Description: Inherited bounding_box function from parent. Creates a bounding box for the geometry primitive
    Inputs: 
        out_box (type:aabb)    - created axis aligned bounding box that is passed to caller as reference.
    Outputs:
        bool    (type:boolean) - returns true if aabb exists
**/
bool cylinder::bounding_box(double t0, double t1, aabb& out_box) const {
	out_box = aabb( vmin(_a, _b) - vec3(radius, radius, radius), 
					vmax(_a, _b) + vec3(radius, radius, radius));
	return true;
}

#endif


// OLD CODE


// // Plane/cap intersection check
// double denom = dot(_axis, r.direction());
// 	if (fabs(denom) > 1e-6) { // ray is parallel to the plane if this is false
// 		// check if it hits the "a" plane (dont hit if "b" is closer)
// 		double t1 = (dot((_a - r.origin()), _axis)/denom);
// 		bool hit_disc_a = (((r.at(t1) - _a).length_squared()) < radius*radius) && (t1>0);
// 		// check if it hits the "b" plane (dont hit if "a" is closer)
// 		double t2 = (dot((_b - r.origin()), _axis)/denom);
// 		bool hit_disc_b = (((r.at(t2) - _b).length_squared()) < radius*radius) && (t2>0);
// 		// if (t1 < t_max && t1 > t_min && (hit_disc_a&&((!hit_disc_b)||(hit_disc_b&&(t1<t2)))) ) {
// 		// 	rec.t = t1;
// 		// 	rec.mat_ptr = mat_ptr;
// 		// 	rec.set_face_normal(r, _axis);
// 		// 	rec.p = r.at(rec.t); //bias value
// 		// 	return true;
// 		// } 
// 		// if (t2 < t_max && t2 > t_min && (hit_disc_b&&((!hit_disc_a)||(hit_disc_a&&(t2<t1))))) {
// 		// 	rec.t = t2;
// 		// 	rec.mat_ptr = mat_ptr;
// 		// 	rec.set_face_normal(r, _axis);
// 		// 	rec.p = r.at(rec.t); //bias value
// 		// 	return true;
// 		// }
// 		double t = (hit_disc_a)?((hit_disc_b)?((t1<t2)?t1:t2):t1):((hit_disc_b)?t2:-1.0);
// 		// double t = (t1<t2)?t1:t2;
// 		// point3 cap_point = (t1<t2)?_a:_b;
// 		// if (t<0) {t = (t1<t2)?t2:t1; cap_point = (t1<t2)?_b:_a;}
// 		// point3 hit_point = r.at(t);
// 		// bool hit_disc = (((hit_point - cap_point).length_squared()) < radius*radius) && (t>0);
// 		if (t < t_max && t > t_min && t>0) {
// 			rec.t = t;
// 			rec.mat_ptr = mat_ptr;
// 			rec.set_face_normal(r, _axis);
// 			rec.p = r.at(t);
// 			return true;
// 		}
// 	}


// /** Function: get_surface_normal(...)
// 	Description: Helper function to get the surface normal of a cylinder
// 	Inputs:  hit_pt (type:point3) - hit point to get the surface normal of
// 	Outputs: vec3   (type:vec3)   - returns the surface normal of the hit_pt
// *
// vec3 cylinder::get_surface_normal(const point3& hit_pt) const {
// 	// check if it hit point is on the upper or lower planes
// 	double d1 = -1.0*dot(_axis, _a);
// 	if (fabs(dot(_axis, hit_pt) + d1) <= 0.000001) return _axis;
// 	double d2 = -1.0*dot(-1.0*_axis, _b);
// 	if (fabs(dot(-1.0*_axis, hit_pt) + d2) <= 0.000001) return -1.0*_axis;

// 	//find the normal vector formed by a, b, and p
// 	vec3 perpend = cross((hit_pt-_b), _axis);
// 	// cylinder direction cross the perpendicular will give the surface normal 
// 	return unit_vector(cross(_axis, perpend));
// }
