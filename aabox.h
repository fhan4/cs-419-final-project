#ifndef AABOX_H
#define AABOX_H

#include "vec3.h"
#include "material.h"
#include "rtweekend.h"
#include "aarect.h"
#include "hittable_list.h"

class aabox : public hittable {
	public:

		/** Function: aabox (Constructor)
			Description: creates an axis aligned box geometry primitive (different from aabb - data struct for bvh)
			Inputs:
				object (type:hittable*) - a geometry primitive to add to the list
			Outputs: NA
		**/
		aabox() {}
		aabox(const point3& p0, const point3& p1, shared_ptr<material> ptr);

		/** Function: hit(...)
			Description: Inherited hit function from parent. Checks if the ray has hit any object within the list.
			Inputs: 
				r     (type:ray)        - casted ray that is checked against all the primitives in the list
				t_min (type:double)     - time minimum
				t_max (type:double)     - time maximum
				rec   (type:hit_record) - record struct used to keep track of hit information if a hit is detected
			Outputs:
				bool  (type:boolean)    - returns if it hit something or not.
		**/
		virtual bool hit(const ray& r, double t_min, double t_max, hit_record& rec) const override;

		/** Function: bounding_box(...)
			Description: Inherited bounding_box function from parent. Creates a bounding box for the geometry primitive
			Inputs: 
				out_box (type:aabb)    - created axis aligned bounding box that is passed to caller as reference.
			Outputs:
				bool    (type:boolean) - returns true if aabb exists
		**/
		virtual bool bounding_box(double t0, double t1, aabb& out_box) const override {
			out_box = aabb(box_min, box_max); return true;
		}

	public:
		point3 box_min;
		point3 box_max;
		hittable_list sides;
};

/** Function: aabox (Constructor)
	Description: creates an axis aligned box geometry primitive (different from aabb - data struct for bvh)
	Inputs:
		object (type:hittable*) - a geometry primitive to add to the list
	Outputs: NA
**/
aabox::aabox(const point3& p0, const point3& p1, shared_ptr<material> ptr) {
	sides.add(make_shared<xy_rect>(p0.x(), p1.x(), p0.y(), p1.y(), p1.z(), ptr));
    sides.add(make_shared<xy_rect>(p0.x(), p1.x(), p0.y(), p1.y(), p0.z(), ptr));

    sides.add(make_shared<xz_rect>(p0.x(), p1.x(), p0.z(), p1.z(), p1.y(), ptr));
    sides.add(make_shared<xz_rect>(p0.x(), p1.x(), p0.z(), p1.z(), p0.y(), ptr));

    sides.add(make_shared<yz_rect>(p0.y(), p1.y(), p0.z(), p1.z(), p1.x(), ptr));
    sides.add(make_shared<yz_rect>(p0.y(), p1.y(), p0.z(), p1.z(), p0.x(), ptr));
}

/** Function: hit(...)
	Description: Inherited hit function from parent. Checks if the ray has hit any object within the list.
	Inputs: 
		r     (type:ray)        - casted ray that is checked against all the primitives in the list
		t_min (type:double)     - time minimum
		t_max (type:double)     - time maximum
		rec   (type:hit_record) - record struct used to keep track of hit information if a hit is detected
	Outputs:
		bool  (type:boolean)    - returns if it hit something or not.
**/
bool aabox::hit(const ray& r, double t_min, double t_max, hit_record& rec) const {
    return sides.hit(r, t_min, t_max, rec);
}


#endif