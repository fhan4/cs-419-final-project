#ifndef RTWEEKEND_H
#define RTWEEKEND_H

#include <cmath>
#include <limits>
#include <memory>
#include <cstdlib>

// Usings

using std::shared_ptr;
using std::make_shared;
using std::sqrt;

// Constants

const double const_inf = std::numeric_limits<double>::infinity();
const double pi = 3.1415926535897932385;
const double refraction_index_air = 1.0;
const double refraction_index_glass = 1.5;

// Utility Functions
inline int log_two(int x)
{
    int i;
    for (i = 0; x > 1; ++i) x /= 2;
    return i;
}

/*
 * Purpose: clamp a value into a range
 * Inputs: value x, range min and max
 * Return value(s): value of x clamped into the range [min, max]
 */
inline double clamp(double x, double min, double max) {
    if (x < min) return min;
    if (x > max) return max;
    return x;
}

/*
 * Purpose: convert degrees to radians
 * Inputs: degrees
 * Return value(s): calculated radians
 */
inline double degrees_to_radians(double degrees) {
    return degrees * pi / 180.0;
}

/*
 * Purpose: generate a random real number in [0,1)
 * Inputs: none
 * Return value(s): generated random number
 */
inline double random_double() {
    return rand() / (RAND_MAX + 1.0);
}

/*
 * Purpose: generate a random real number in [min,max)
 * Inputs: range min and max
 * Return value(s): generated random number
 */
inline double random_double(double min, double max) {
    return min + (max-min)*random_double();
}

/*
 * Purpose: generate a random integer in [min,max)
 * Inputs: range min and max
 * Return value(s): generated integer
 */
inline int random_int(int min, int max) {
    // Returns a random integer in [min,max].
    return static_cast<int>(random_double(min, max+1));
}

// Common Headers

#include "ray.h"
#include "vec3.h"

#endif