#ifndef HITTABLE_H
#define HITTABLE_H

#include "ray.h"
#include "aabb.h"
#include "rtweekend.h"

class material;
// hit_record struct to keep a ray's record of hitting hittables
struct hit_record {
    point3 p;
    double u;
    double v;
    vec3 normal;
    double t;
    bool front_face;
    color hittable_color;
    shared_ptr<material> mat_ptr;

    /*
     * Purpose: determine whether the front face is hit and set the sign of the normal
     * Inputs: a ray r and outward normal outward_normal
     * Return value(s): none
     */
    inline void set_face_normal(const ray& r, const vec3& outward_normal) {
        front_face = dot(r.direction(), outward_normal) < 0;
        normal = front_face ? outward_normal :-outward_normal;
    }
};

// hittable class to be overridden by specific objects in the scene
class hittable {
public:
    virtual bool hit(const ray& r, double t_min, double t_max, hit_record& rec) const = 0;
    virtual bool bounding_box(double time0, double time1, aabb& output_box) const = 0;
};

#endif