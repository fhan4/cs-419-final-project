#ifndef HITTABLE_LIST_H
#define HITTABLE_LIST_H
#include "material.h"
#include "hittable.h"
#include "aabb.h"

#include <memory>
#include <vector>

using std::shared_ptr;
using std::make_shared;

//list of hittables
class hittable_list : public hittable {
public:
    hittable_list() {}
    hittable_list(shared_ptr<hittable> object) { add(object); }

    /*
     * Purpose: add an object to the list
     * Inputs: hittable object
     * Return value(s): none
     */
    void add(shared_ptr<hittable> object) { objects.push_back(object); }
    void pop() { objects.pop_back(); }
    virtual bool hit(
            const ray& r, double t_min, double t_max, hit_record& rec) const override;

    virtual bool hit_without_medium(
            const ray& r, double t_min, double t_max, hit_record& rec) const;

    virtual bool bounding_box(
            double time0, double time1, aabb& output_box) const override;

    virtual bool hit_hard_shadow(const ray& r) const;

public:
    std::vector<shared_ptr<hittable>> objects;
};

/*
 * Purpose: determines if a given ray hit anything in the hittable list
 * Inputs: a ray r, range t_min and t_max, hit record rec
 * Return value(s): boolean value based on whether the input ray hit anything
 */
bool hittable_list::hit(const ray& r, double t_min, double t_max, hit_record& rec) const {
    hit_record temp_rec;
    bool hit_anything = false;
    auto closest_so_far = t_max;

    for (const auto& object : objects) {
        if (object->hit(r, t_min, closest_so_far, temp_rec)) {
            hit_anything = true;
            closest_so_far = temp_rec.t;
            rec = temp_rec;
        }
        
    }
    return hit_anything;
}


/*
 * Purpose: determines if a given ray hit anything in the hittable list
 * Inputs: a ray r, range t_min and t_max, hit record rec
 * Return value(s): boolean value based on whether the input ray hit anything
 */
bool hittable_list::hit_without_medium(const ray& r, double t_min, double t_max, hit_record& rec) const {
    hit_record temp_rec;
    bool hit_anything = false;
    auto closest_so_far = t_max;

    for (const auto& object : objects) {
        if (object->hit(r, t_min, closest_so_far, temp_rec)) {
            if (temp_rec.mat_ptr->get_type() == 'P'){
                continue;
            }
            hit_anything = true;
            closest_so_far = temp_rec.t;
            rec = temp_rec;
        }
        
    }
    return hit_anything;
}



/*
 * Purpose: determines whether a ray hits shadows of any object in the list
 * Inputs: ray r
 * Return value(s): boolean value based on whether the input ray hit shadows
 */
bool hittable_list::hit_hard_shadow(const ray& r) const{
    hit_record record;

    for (const auto& object : objects) {
        if (object->hit(r, 0.00001, 1.0, record)) {
            return true;
        }
    }

    return false;
}

bool hittable_list::bounding_box(double time0, double time1, aabb& output_box) const {
    if (objects.empty()) return false;

    aabb temp_box;
    bool first_box = true;

    for (const auto& object : objects) {
        if (!object->bounding_box(time0, time1, temp_box)) return false;
        output_box = first_box ? temp_box : surrounding_box(output_box, temp_box);
        first_box = false;
    }

    return true;
}

#endif
