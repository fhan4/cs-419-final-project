#ifndef CAMERA_H
#define CAMERA_H

#include "rtweekend.h"

/*
 * The camera class to create a positionable camera
 */
class camera {
public:
    /*
     * Purpose: constructor of the camera
     * Inputs: look-from point lookfrom, look-at point lookat, vec3 vup to determine the camera's angle,
     *         vertical field-of-view in degrees vfov, and camera aspect ratio
     * Return value(s): none
     */
    camera(
            point3 lookfrom,
            point3 lookat,
            vec3   vup,
            double vfov,
            double aspect_ratio
    ) {
        auto theta = degrees_to_radians(vfov);
        auto h = tan(theta/2);
        auto viewport_height = 1.0 * h;
        auto viewport_width = aspect_ratio * viewport_height;

        auto w = unit_vector(lookfrom - lookat);
        auto u = unit_vector(cross(vup, w));
        auto v = cross(w, u);

        origin = lookfrom;
        target = lookat;
        horizontal = viewport_width * u;
        vertical = viewport_height * v;
        lower_left_corner = origin - horizontal/2 - vertical/2 - w;
    }

    /*
     * Purpose: get a ray for perspective rendering
     * Inputs: coordinates s and t
     * Return value(s): the generated ray for perspective rendering
     */
    ray get_ray_perspective(double s, double t) const {
        return ray(origin, lower_left_corner + s*horizontal + t*vertical - origin);
    }

    /*
     * Purpose: get a ray for orthographic rendering
     * Inputs: coordinates s and t
     * Return value(s): the generated ray for orthographic rendering
     */
    ray get_ray_orthographic(double s, double t) const {
        return ray(origin + (s-0.5) * horizontal + (t-0.5) * vertical, unit_vector(target));
    }

private:
    point3 origin;
    point3 target;
    point3 lower_left_corner;
    vec3 horizontal;
    vec3 vertical;
};
#endif
