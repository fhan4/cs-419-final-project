#ifndef SAMPLING_H
#define SAMPLING_H

#include "rtweekend.h"

#include <algorithm>
#include <cmath>
#include <ctime>
#include <iostream>
#include <vector>

using std::vector;

// class for multi-jittered sampling
class sampling {
private:
    int grid;
    int coarse_grid;
    double grid_unit;
    double coarse_grid_unit;

public:
    sampling(int g) {
        grid = g;
        coarse_grid = static_cast<int> (std::sqrt((double)grid));
        grid_unit = 1.0 / grid;
        coarse_grid_unit = 1.0 / coarse_grid;
    }

    /*
     * Purpose: generate sample points given row and column
     * Inputs: row and column numbers
     * Return value(s): list of sample points in the form of vector<vector<double>>
     */
    vector<vector<double>> get_samples(int row, int col) const {
        vector<vector<double>> sample_points;
        vector<vector<int>> row_ceils;
        vector<vector<int>> col_ceils;

        for (int i = 0; i < coarse_grid; i++) {
            vector<int> line;
            for (int j = 0; j < coarse_grid; j++) {
                line.push_back(j);
            }
            row_ceils.push_back(line);
            col_ceils.push_back(line);
        }

        for (int i = 0; i < coarse_grid; i++) {
            for (int j = 0; j < coarse_grid; j++) {
                vector<double> sample_point;

                int row_idx = random_int(0, row_ceils[i].size() - 1);
                double row_min = row + i * coarse_grid_unit + row_ceils[i][row_idx] * grid_unit;
                sample_point.push_back(random_double(row_min, row_min + grid_unit));
                int col_idx = random_int(0, col_ceils[j].size() - 1);
                double col_min = col + j * coarse_grid_unit + col_ceils[j][col_idx] * grid_unit;
                sample_point.push_back(random_double(col_min, col_min + grid_unit));

                row_ceils[i].erase(row_ceils[i].begin() + row_idx);
                col_ceils[j].erase(col_ceils[j].begin() + col_idx);

                sample_points.push_back(sample_point);
            }
        }

        return sample_points;
    }
};

#endif
