#ifndef AARECT_H
#define AARECT_H

#include "rtweekend.h"

#include "hittable.h"

class xy_rect : public hittable {
public:
    xy_rect() {}

    xy_rect(double _x0, double _x1, double _y0, double _y1, double _k,
            shared_ptr<material> mat)
            : x0(_x0), x1(_x1), y0(_y0), y1(_y1), k(_k), mp(mat) {};

    virtual bool hit(const ray& r, double t_min, double t_max, hit_record& rec) const override;

    virtual bool bounding_box(double time0, double time1, aabb& output_box) const override {
        // The bounding box must have non-zero width in each dimension, so pad the Z
        // dimension a small amount.
        output_box = aabb(point3(x0,y0, k-0.0001), point3(x1, y1, k+0.0001));
        return true;
    }

public:
    shared_ptr<material> mp;
    double x0, x1, y0, y1, k;
};

bool xy_rect::hit(const ray& r, double t_min, double t_max, hit_record& rec) const {
    auto t = (k-r.origin().z()) / r.direction().z();
    if (t < t_min || t > t_max)
        return false;
    auto x = r.origin().x() + t*r.direction().x();
    auto y = r.origin().y() + t*r.direction().y();
    if (x < x0 || x > x1 || y < y0 || y > y1)
        return false;
    rec.u = (x-x0)/(x1-x0);
    rec.v = (y-y0)/(y1-y0);
    rec.t = t;
    auto outward_normal = vec3(0, 0, 1);
    rec.set_face_normal(r, outward_normal);
    rec.mat_ptr = mp;
    rec.p = r.at(t);
    return true;
}

class xz_rect : public hittable {
    public:
        /** Function: xz_rect(...) (Constructor)
            Description: Creates a rectangle that is aligned with the x and z axis
            Inputs:  
                _x0, _x1 (type:doubles)  - the bounding x coordinates for the rectangle
                _z0, _z1 (type:doubles)  - the bounding z coordinates for the rectangle
                _k       (type:double)   - the depth of the rect along the y axis
                mat      (type:material) - material of the geometry object  
            Outputs: NA
        **/
        xz_rect() {}
        xz_rect(double _x0, double _x1, double _z0, double _z1, double _k, shared_ptr<material> mat)
            : x0(_x0), x1(_x1), z0(_z0), z1(_z1), y(_k), m(mat) {};

        /** Function: hit(...)
            Description: Inherited hit function from parent. Checks if the ray has hit the object
            Inputs: 
                r     (type:ray)        - casted ray that is checked against the geometry primitive
                t_min (type:double)     - time minimum
                t_max (type:double)     - time maximum
                rec   (type:hit_record) - record struct used to keep track of hit information if a hit is detected
            Outputs:
                bool  (type:boolean)    - returns if it hit something or not.
        **/
        virtual bool hit(const ray& r, double t_min, double t_max, hit_record& rec) const override;

        /** Function: bounding_box(...)
            Description: Inherited bounding_box function from parent. Creates a bounding box for the geometry primitive
            Inputs:  out_box (type:aabb)    - created axis aligned bounding box that is passed to caller as reference.
            Outputs: bool    (type:boolean) - returns true if aabb exists.
        **/
        virtual bool bounding_box(double t0, double t1, aabb& out_box) const override {
            out_box = aabb(point3(x0,y-0.0001,z0), point3(x1, y+0.0001, z1));
            return true;
        }

    public:
        double x0, x1, z0, z1, y;
        shared_ptr<material> m;
};

class yz_rect : public hittable {
    public:
        /** Function: yz_rect(...) (Constructor)
            Description: Creates a rectangle that is aligned with the x and y axis
            Inputs:  
                _y0, _y1 (type:doubles)  - the bounding y coordinates for the rectangle
                _z0, _z1 (type:doubles)  - the bounding z coordinates for the rectangle
                _k       (type:double)   - the depth of the rect along the x axis
                mat      (type:material) - material of the geometry object  
            Outputs: NA
        **/
        yz_rect() {}
        yz_rect(double _y0, double _y1, double _z0, double _z1, double _k, shared_ptr<material> mat)
            : y0(_y0), y1(_y1), z0(_z0), z1(_z1), x(_k), m(mat) {};

        /** Function: hit(...)
            Description: Inherited hit function from parent. Checks if the ray has hit the object
            Inputs: 
                r     (type:ray)        - casted ray that is checked against the geometry primitive
                t_min (type:double)     - time minimum
                t_max (type:double)     - time maximum
                rec   (type:hit_record) - record struct used to keep track of hit information if a hit is detected
            Outputs:
                bool  (type:boolean)    - returns if it hit something or not.
        **/
        virtual bool hit(const ray& r, double t_min, double t_max, hit_record& rec) const override;

        /** Function: bounding_box(...)
            Description: Inherited bounding_box function from parent. Creates a bounding box for the geometry primitive
            Inputs: 
                out_box (type:aabb)    - created axis aligned bounding box that is passed to caller as reference.
            Outputs:
                bool    (type:boolean) - returns true if aabb exists.
        **/
        virtual bool bounding_box(double t0, double t1, aabb& out_box) const override {
            out_box = aabb(point3(x-0.0001, y0, z0), point3(x+0.0001, y1, z1));
            return true;
        }

    public:
        double y0, y1, z0, z1, x;
        shared_ptr<material> m;
};

bool xz_rect::hit(const ray& r, double t_min, double t_max, hit_record& rec) const {
    double t = (y-r.origin().y()) / r.direction().y();
    if (t < t_min || t > t_max) return false;
    double x = r.origin().x() + t*r.direction().x();
    double z = r.origin().z() + t*r.direction().z();
    if (x < x0 || x > x1 || z < z0 || z > z1) return false;
    
    rec.t = t;
    rec.set_face_normal(r, vec3(0, 1, 0));
    rec.mat_ptr = m;
    rec.p = r.at(t);
    return true;
}

bool yz_rect::hit(const ray& r, double t_min, double t_max, hit_record& rec) const {
    double t = (x-r.origin().x()) / r.direction().x();
    if (t < t_min || t > t_max)   return false;
    double y = r.origin().y() + t*r.direction().y();
    double z = r.origin().z() + t*r.direction().z();
    if (y < y0 || y > y1 || z < z0 || z > z1) return false;

    rec.t = t;
    rec.set_face_normal(r, vec3(1, 0, 0));
    rec.mat_ptr = m;
    rec.p = r.at(t);
    return true;
}

struct Point2D
{
    double x;
    double y;
};

class aa_rect : public hittable
{
public:
    aa_rect() {}
    aa_rect(double _x0, double _x1, double _y0, double _y1, double _k, shared_ptr<material> mat, int ax)
        : x0(_x0), x1(_x1), y0(_y0), y1(_y1), k(_k), axis(ax), mp(mat){};

    virtual bool hit(const ray &r, double t0, double t1, hit_record &rec) const;
    virtual bool bounding_box(double time0, double time1, aabb &output_box) const
    {
        // The bounding box must have non-zero width in each dimension, so pad the Z
        // dimension a small amount.
        switch (axis)
        {
        case 0:
            output_box = aabb(point3(k - 0.0001, x0, y0), point3(k + 0.0001, x1, y1));
            break;
        case 1:
            output_box = aabb(point3(x0, k - 0.0001, y0), point3(x1, k + 0.0001, y1));
            break;
        case 2:
            output_box = aabb(point3(x0, y0, k - 0.0001), point3(x1, y1, k + 0.0001));
            break;

        default:
            break;
        }
        return true;
    }
    vector<point3> getSamples(int N)
    {
        /*
        Get N multi-jittered samples on the aarect given fine-grid size N
        Argument(s):
            int N: The fine-grid size
        Return:
            vector<Point3D>: vector of Point3D objects (x, y, z coordinates) for all the generated samples in the aarect.

    */
        double n = sqrt(N);

        auto p = vector<Point2D>(N);
        auto subcell_width = 1.0 / N;

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                p[i * n + j].x = i * n * subcell_width + j * subcell_width + random_double(0, subcell_width);
                p[i * n + j].y = j * n * subcell_width + i * subcell_width + random_double(0, subcell_width);
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                int k = random_int(j, n - 1);
                auto temp = p[i * n + j].x;
                p[i * n + j].x = p[i * n + k].x;
                p[i * n + k].x = temp;
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                int k = random_int(j, n - 1);
                auto temp = p[j * n + i].y;
                p[j * n + i].y = p[j * n + k].y;
                p[j * n + k].y = temp;
            }
        }
        auto p3d = vector<point3>(N);
        for (int i = 0; i < N; i++)
        {
            if (axis == 0)
            {
                p3d[i].e[0] = k;
                p3d[i].e[1] = (p[i].x) * (x1 - x0) + x0;
                p3d[i].e[2] = (p[i].y) * (y1 - y0) + y0;
            }
            if (axis == 1)
            {
                p3d[i].e[0] = (p[i].x) * (x1 - x0) + x0;
                p3d[i].e[1] = k;
                p3d[i].e[2] = (p[i].y) * (y1 - y0) + y0;
            }
            if (axis == 2)
            {
                p3d[i].e[0] = (p[i].x) * (x1 - x0) + x0;
                p3d[i].e[1] = (p[i].y) * (y1 - y0) + y0;
                p3d[i].e[2] = k;
            }
        }
        return p3d;
    }

public:
    shared_ptr<material> mp;
    double x0, x1, y0, y1, k; // k is the z value for the plane of the rect
    int axis;                 // 0, 1, 2 are x, y, z
};

bool aa_rect::hit(const ray &r, double t0, double t1, hit_record &rec) const
{
    double t;
    vec3 outward_normal;
    double x, y;
    switch (axis)
    {
    case 0:
        t = (k - r.origin().x()) / r.direction().x();

        if (t < t0 || t > t1)
            return false;

        x = r.origin().y() + t * r.direction().y();
        y = r.origin().z() + t * r.direction().z();
        if (x < x0 || x > x1 || y < y0 || y > y1)
            return false;

        outward_normal = vec3(1, 0, 0);
        break;
    case 1:
        t = (k - r.origin().y()) / r.direction().y();

        if (t < t0 || t > t1)
            return false;

        x = r.origin().x() + t * r.direction().x();
        y = r.origin().z() + t * r.direction().z();
        if (x < x0 || x > x1 || y < y0 || y > y1)
            return false;

        outward_normal = vec3(0, 1, 0);
        break;
    case 2:
        t = (k - r.origin().z()) / r.direction().z();

        if (t < t0 || t > t1)
            return false;

        x = r.origin().x() + t * r.direction().x();
        y = r.origin().y() + t * r.direction().y();
        if (x < x0 || x > x1 || y < y0 || y > y1)
            return false;

        outward_normal = vec3(0, 0, 1);
        break;
    };
    rec.t = t;
    rec.set_face_normal(r, outward_normal);
    rec.mat_ptr = mp;
    rec.p = r.at(t);
    return true;
}


#endif