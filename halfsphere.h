#ifndef HALFSPHERE_H
#define HALFSPHERE_H

#include "hittable.h"
#include "vec3.h"

class halfsphere : public hittable {
public:
    halfsphere() {}
    //halfsphere(point3 cen, double r, color c) : center(cen), radius(r), hittable_color(c) {};
    halfsphere(point3 cen, double r, shared_ptr<material> m)
            : center(cen), radius(r), mat_ptr(m) {};

    virtual bool hit(
            const ray& r, double t_min, double t_max, hit_record& rec) const override;
    virtual bool bounding_box(double time0, double time1, aabb& output_box) const override;

public:
    point3 center;
    double radius;
    //color hittable_color;
    shared_ptr<material> mat_ptr;

    static void set_u_v(const point3& p, double& u, double& v) {
        auto theta = acos(-p.y());
        auto phi = atan2(-p.z(), p.x()) + pi;
        u = phi / (2 * pi);
        v = theta / pi;
    }
};

/*
 * Purpose: hit function for the halfsphere
 * Inputs: ray r, range t_min and t_max, hit record rec
 * Return value(s): boolean value based on if the ray hits the halfsphere, record data in rec if the ray hits
 */
bool halfsphere::hit(const ray& r, double t_min, double t_max, hit_record& rec) const {
    vec3 oc = r.origin() - center;
    auto a = r.direction().length_squared();
    auto half_b = dot(oc, r.direction());
    auto c = oc.length_squared() - radius*radius;

    auto discriminant = half_b*half_b - a*c;
    if (discriminant < 0) return false;
    auto sqrtd = sqrt(discriminant);

    // Find the nearest root that lies in the acceptable range.
    auto root = (-half_b - sqrtd) / a;
    if (root < t_min || t_max < root) {
        root = (-half_b + sqrtd) / a;
        if (root < t_min || t_max < root)
            return false;
    }

    rec.t = root;
    rec.p = r.at(rec.t);
    vec3 outward_normal = (rec.p - center) / radius;
    rec.set_face_normal(r, outward_normal);
    //rec.hittable_color = hittable_color;
    set_u_v(outward_normal, rec.u, rec.v);
    rec.mat_ptr = mat_ptr;
    
    if (rec.p.e[1] >=center.e[1]){
       return false;
    } 
    return true;
}

/*
 * Purpose: bounding box function for the halfsphere
 * Inputs: time range time0 and time1, pointer to bounding box output_box
 * Return value(s): calculate the bounding box, return true
 */
bool halfsphere::bounding_box(double time0, double time1, aabb& output_box) const {
    output_box = aabb(
            center - vec3(radius, radius, radius),
            center + vec3(radius, radius, radius));
    return true;
}

#endif
