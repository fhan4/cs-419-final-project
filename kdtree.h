#ifndef KDTREE_H
#define KDTREE_H

#include "vec3.h"
#include "aabb.h"
#include "rtweekend.h"

struct photon {
    point3 position;
    vec3 power;
    vec3 direction;
};


bool comp_x(const photon* a, const photon* b) { return a->position.e[0] < b->position.e[0]; }
bool comp_y(const photon* a, const photon* b) { return a->position.e[1] < b->position.e[1]; }
bool comp_z(const photon* a, const photon* b) { return a->position.e[2] < b->position.e[2]; }

struct KDTree {
    photon** photons;
    int* axes;
    int size;
    void build(photon** buf, int num)
    {
        size = num;
        photons = new photon*[size];
        axes = new int[size / 2];
        build_node(buf, 0, size);
    }
    void nearest(photon** selected_photons, double* square_distances, int estimate, const vec3& p)
    {
        for (int i = 0; i < estimate; ++i) {
            selected_photons[i] = nullptr;
            square_distances[i] = const_inf;
        }
        traverse(selected_photons, square_distances, estimate, p, 0);
    }
    static int mid_point(int total)
    {
        int log = log_two(total);
        int largest_power = pow(2, log);
        if (largest_power >= 2 && total - largest_power >= largest_power / 2)
            return largest_power - 1;
        else
            return total - largest_power / 2;
    }
    static int largest_axis(photon** selected_photons, int photon_num)
    {
        aabb bounding_box = aabb(selected_photons[0]->position, selected_photons[0]->position);
        for (int i = 1; i < photon_num; ++i)
            bounding_box = surrounding_box(aabb(selected_photons[i]->position, selected_photons[i]->position), bounding_box);
        vec3 w = bounding_box.max() - bounding_box.min();
        return w.e[0] > w.e[1] ? (w.e[0] > w.e[2] ? 0 : 2) : (w.e[1] > w.e[2] ? 1 : 2);
    }
    void build_node(photon** selected_photons, int index, int photon_num)
    {
        if (photon_num == 1)
            photons[index] = *selected_photons;
        else if (photon_num > 1) {
            int axis = largest_axis(selected_photons, photon_num);
            std::sort(selected_photons, selected_photons + photon_num, (axis == 0) ? comp_x : (axis == 1) ? comp_y : comp_z);
            int mid = mid_point(photon_num);
            photons[index] = selected_photons[mid];
            axes[index] = axis;
            build_node(selected_photons, index * 2 + 1, mid);
            build_node(selected_photons + mid + 1, index * 2 + 2, photon_num - mid - 1);
        }
    }
    void traverse(photon** selected_photons, double* square_distances, int estimate, const vec3& p, int index)
    {
        if (index >= size)
            return;
        else if (index * 2 + 1 < size) {
            double axis_distance = p.e[axes[index]] - photons[index]->position.e[axes[index]];
            if (axis_distance < 0) {
                traverse(selected_photons, square_distances, estimate, p, index * 2 + 1);
                if (axis_distance * axis_distance < square_distances[estimate - 1])
                    traverse(selected_photons, square_distances, estimate, p, index * 2 + 2);
            }
            else {
                traverse(selected_photons, square_distances, estimate, p, index * 2 + 2);
                if (axis_distance * axis_distance < square_distances[estimate - 1])
                    traverse(selected_photons, square_distances, estimate, p, index * 2 + 1);
            }
        }
        double square_distance = (photons[index]->position - p).length_squared();
        if (square_distance >= square_distances[estimate - 1])
            return;
        int left = 0;
        int right = estimate;
        while (left < right) {
            int mid = (left + right) / 2;
            if (square_distance < square_distances[mid])
                right = mid;
            else left = mid + 1;
        }
        for (int j = estimate - 1; j > left; j--) {
            selected_photons[j] = selected_photons[j - 1];
            square_distances[j] = square_distances[j - 1];
        }
        selected_photons[left] = photons[index];
        square_distances[left] = square_distance;
    }
};

#endif
