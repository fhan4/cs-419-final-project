#include "rtweekend.h"
#include "camera.h"
#include "color.h"
#include "hittable_list.h"
#include "sphere.h"
#include "halfsphere.h"
#include "sampling.h"
#include "material.h"
#include "aarect.h"
#include "kdtree.h"
#include <math.h>
#include "bvh.h"
#include "csg.h"
#include "aabox.h"
#include "cylinder.h"
#include <fstream>
#include <iostream>
#include <chrono>

using namespace std;

vec3 light_power = vec3(30000, 30000, 30000);
const auto aspect_ratio = 1.0;
const int image_width = 400;
const int image_height = static_cast<int>(image_width * aspect_ratio);
int photon_emit_size = 1024;
int num_caustic_photons = 10000;
int num_medium_photons = 10000;
const int samples_per_pixel = 4;
const int medium_near_estimate_size = 50;
const int near_estimate_size = 50;
const int caustic_near_estimate_size = 50;
const int volumn_rendering_distance = 5;
const int radius_of_fogs = 110;
const double phase_func_g = 0.45;
auto material_light = make_shared<diffuse_light>(color(1, 1, 1));
auto area_light = make_shared<aa_rect>(-10, 10, -30, -10, 99.9, material_light, 1);
auto light_samples = area_light->getSamples(photon_emit_size);
auto caustic_light_samples = area_light->getSamples(num_caustic_photons);
auto medium_light_samples = area_light->getSamples(num_medium_photons);
std::shared_ptr<material> medium_material_pointer;

color emit_photons(std::vector<photon*>& photons, hittable_list& world, const ray& r, int depth, const vec3& power)
{
    depth--;
    hit_record rec;
    ray scattered;
    color attenuation;
    if ((depth <= 0) || !world.hit(r, 0.0001, const_inf, rec))
        return {0, 0, 0};
    const vec3 material_color = rec.mat_ptr->get_color();
    const vec3 unit_normal = unit_vector(rec.normal);
    const double s = rec.mat_ptr->get_specular_coefficient();
    const double d = rec.mat_ptr->get_diffuse_coefficient();
    double roulette = random_double();
    double pr = std::max(material_color.e[0], std::max(material_color.e[1], material_color.e[2]));
    if (roulette >= pr * (s + d))
        return {0, 0, 0};

    
    rec.mat_ptr->scatter(r, rec, attenuation, scattered);
    if (roulette < pr * d) {
        auto *ph = new photon;
        ph->position = rec.p;
        ph->power = power;
        photons.push_back(ph);
        // double random_rad = 2 * pi * random_double();
        // double random_fraction = random_double();
        // vec3 w = unit_normal;
        // vec3 u = unit_vector(cross((fabs(w.e[0]) > fabs(w.e[1]) ? vec3(0, 1, 0) : vec3(1, 0, 0)), w));
        // vec3 v = cross(w, u);
        // vec3 d = (u * cos(random_rad) * sqrt(random_fraction) + v * sin(random_rad) * sqrt(random_fraction) + w * sqrt(1 - random_fraction));
        return emit_photons(photons, world, scattered, depth, material_color * power * (1.0 / pr));
    }
    else {
        return emit_photons(photons, world, scattered, depth, material_color * power) * attenuation;
    }
}

color emit_photons_caustic(std::vector<photon*>& photons, hittable_list& world, const ray& r, int depth, const vec3& power)
{
    depth--;
    hit_record rec;
    ray scattered;
    color attenuation;
    if ((depth <= 0) || !world.hit(r, 0.0001, const_inf, rec))
        return {0, 0, 0};
    const vec3 material_color = rec.mat_ptr->get_color();
    const vec3 unit_normal = unit_vector(rec.normal);
    const double s = rec.mat_ptr->get_specular_coefficient();
    const double d = rec.mat_ptr->get_diffuse_coefficient();
    
    rec.mat_ptr->scatter(r, rec, attenuation, scattered);
    if (d > 0) {
        auto *ph = new photon;
        ph->position = rec.p;
        ph->power = power;
        photons.push_back(ph);
    }
    if (s > 0) {
        return emit_photons_caustic(photons, world, scattered, depth, material_color * power) * attenuation;
    }
    return color(0, 0, 0);
}

color ray_color_photon(KDTree& global_map, hittable_list& world, const ray& r, int depth, int near_estimate_size)
{
    depth--;
    hit_record rec;
    if ((depth <= 0) || !world.hit_without_medium(r, 0.0001, const_inf, rec))
        return {0, 0, 0};
    const vec3 material_color = rec.mat_ptr->get_color();
    const double s = rec.mat_ptr->get_specular_coefficient();
    const double d = rec.mat_ptr->get_diffuse_coefficient();
    ray scattered;
    color attenuation;
    color ray_color = vec3(0, 0, 0);
    if (!rec.mat_ptr->scatter(r, rec, attenuation, scattered)){
        return rec.mat_ptr->emitted(rec.u, rec.v, rec.p);

    }
    
    // Direct illumination
    int N = 4;
    auto direct_samples = area_light->getSamples(N);
    color direct_illumination = color(0, 0, 0);
    for (auto p: direct_samples) {
        ray shadow_ray = ray(rec.p, p-rec.p);
        hit_record shadow_rec;
        bool inShadow = world.hit_without_medium(shadow_ray, 0.001, 1.001, shadow_rec);
        if (shadow_rec.t == 1) {
            // cerr <<"here\n";
            // cerr << attenuation << endl;
            // cerr << shadow_rec.mat_ptr->emitted(rec.u, rec.v, rec.p) << endl;
            // cerr << (1 / (p - rec.p).length()) << endl<<endl;
            auto r = sqrt(sqrt((1 / (p - rec.p).length())));
            direct_illumination += 0.5 * attenuation * shadow_rec.mat_ptr->emitted(rec.u, rec.v, rec.p) * r;
        }
    }
    direct_illumination /= N;
    // return  direct_illumination;

    // Global map illumination
    color global_map_illumination = color(0, 0, 0);
    if (d > 0) {
        auto** nearest = (photon**)malloc(sizeof(photon*) * near_estimate_size);
        auto* square_distances = (double*)malloc(sizeof(double) * near_estimate_size);
        global_map.nearest(nearest, square_distances, near_estimate_size, rec.p);
        vec3 power = vec3(0, 0, 0);
        int i;
        for (i = 0; i < near_estimate_size; i++) {
            photon* photon_near = nearest[i];
            if (!photon_near) break;
            power += photon_near->power;
        }
        global_map_illumination += d * power * material_color * (1.0 / (pi * square_distances[i - 1]));
        free(nearest);
        free(square_distances);
    }
    if (s > 0) {
        global_map_illumination += s * ray_color_photon(global_map, world, scattered, depth, near_estimate_size) * attenuation;
    }

    return ray_color + direct_illumination + global_map_illumination;
}

color ray_color_caustic(KDTree& caustic_map, hittable_list& world, const ray& r, int caustic_near_estimate_size) {
    hit_record rec;
    if (!world.hit_without_medium(r, 0.0001, const_inf, rec))
        return {0, 0, 0};
    const vec3 material_color = rec.mat_ptr->get_color();
    const double s = rec.mat_ptr->get_specular_coefficient();
    const double d = rec.mat_ptr->get_diffuse_coefficient();
    ray scattered;
    color attenuation;
    if (!rec.mat_ptr->scatter(r, rec, attenuation, scattered)){
        return color(0, 0, 0);
    }
    //Caustic map illumination
    color caustic_map_illumination = color(0, 0, 0);
    if (d > 0) {
        auto** nearest = (photon**)malloc(sizeof(photon*) * caustic_near_estimate_size);
        auto* square_distances = (double*)malloc(sizeof(double) * caustic_near_estimate_size);
        caustic_map.nearest(nearest, square_distances, caustic_near_estimate_size, rec.p);
        vec3 power = vec3(0, 0, 0);
        int i;
        for (i = 0; i < caustic_near_estimate_size; i++) {
            photon* photon_near = nearest[i];
            if (!photon_near) break;
            power += photon_near->power;
        }
        caustic_map_illumination += d * power * material_color * (1.0 / (pi * square_distances[i - 1]));
        free(nearest);
        free(square_distances);
    }
    return caustic_map_illumination;
}

color ray_color_medium(KDTree& medium_map, hittable_list& world, const ray& r, int medium_near_estimate_size) {
    hit_record rec;
    if (!world.hit_without_medium(r, 0.0001, const_inf, rec))
        return {0, 0, 0};

    ray scattered;
    color attenuation;
    if (!rec.mat_ptr->scatter(r, rec, attenuation, scattered)){
        return color(0, 0, 0);
    }
    
    double theta,phase;
    //Medium map illumination
    hit_record rec_front,rec_back;
    ray ray_back = ray(rec.p,-1 * r.dir);
    color medium_map_illumination = color(0, 0, 0);
    if ( world.hit(r, 0.0001, const_inf, rec_front) && rec.t > rec_front.t){
        int start_t;
        if ((rec.p- point3(0, 100, -20)).length_squared() <= radius_of_fogs* radius_of_fogs && rec.p.e[1]<=100){
            start_t = rec.t;
        }
        else if (world.hit(ray_back,0.0001, const_inf, rec_back)){ // && rec_back.t<rec.t && (rec.t - rec_back.t) > rec_front.t
            start_t = rec.t - rec_back.t;
        }else{
            start_t=rec.t;
        }
        for (int t = start_t  ; t>rec_front.t; t = t - volumn_rendering_distance){
            point3 query_point = r.orig + t * r.dir;

            //global illumination
            int N = 4;
            auto direct_samples = area_light->getSamples(N);
            color direct_illumination = color(0, 0, 0);
            color indirect_illumination = color(0, 0, 0);
            for (auto p: direct_samples) {
                ray shadow_ray = ray(query_point, p - query_point);
                hit_record shadow_rec, shadowMedium_rec;
                bool inShadow = world.hit_without_medium(shadow_ray, 0.001, 1.001, shadow_rec);
                if (shadow_rec.t == 1) {
                    theta = dot(unit_vector(query_point - p),unit_vector((rec_front.p - query_point)));
                    phase = (1-phase_func_g*phase_func_g)/(4*pi*pow((1+phase_func_g*phase_func_g-2*phase_func_g*theta),1.5));
                    direct_illumination += 0.25  * shadow_rec.mat_ptr->emitted(rec.u, rec.v, rec.p) * pow(2.73,- 0.025*(p - query_point).length()) 
                    * phase * pow(2.73,- 0.025*(rec_front.p - query_point).length()); //0.005-0.010/ 10000
                }
            }

            //indirect illumination
            auto** nearest = (photon**)malloc(sizeof(photon*) * medium_near_estimate_size);
            auto* square_distances = (double*)malloc(sizeof(double) * medium_near_estimate_size);
            medium_map.nearest(nearest, square_distances, medium_near_estimate_size, query_point);
            vec3 power = vec3(0, 0, 0);
            int i;
            for (i = 0; i < medium_near_estimate_size; i++) {
                photon* photon_near = nearest[i];
                theta = dot(unit_vector(photon_near->direction),unit_vector(rec_front.p - query_point));
                phase = (1-phase_func_g*phase_func_g)/(4*pi*pow((1+phase_func_g*phase_func_g-2*phase_func_g*theta),1.5));
                if (!photon_near) break;
                power += photon_near->power ;
            }
            indirect_illumination = power  * (3.0 / (4.0 * pi * pow(square_distances[i - 1],1.5))) * pow(2.73,- 0.005*(rec_front.p - query_point).length());
            free(nearest);
            free(square_distances);

            medium_map_illumination +=  indirect_illumination + direct_illumination; // 
          }
    }


    return medium_map_illumination;
}


color emit_photons_medium(std::vector<photon*>& photons, hittable_list& world, const ray& r ,int depth, const vec3& power)
{
    depth--;
    hit_record rec;
    ray scattered;
    color attenuation;
    point3 current_hit;
    ray unit_ray = ray(r.orig, unit_vector(r.dir));
    const double sigma_t = 0.02;
    const double albedo = 0.75;
    double roulette = random_double();
    double estimate_distance = - log(roulette)/sigma_t;
    if (depth <= 0){
        return {0, 0, 0};
    }
    if (!world.hit_without_medium(r, 0.0001, estimate_distance, rec)){ //not hit in allow distance, consider absorb or scatter
       roulette = random_double();
       if (roulette > albedo){
           return {0,0,0};  //terminate the ray getting absorb
       }
       else {  //scatter the ray
           hit_record rec2;
           current_hit = r.orig + estimate_distance * unit_ray.dir;
           rec2.p = current_hit;
           medium_material_pointer->scatter(unit_ray,rec2,attenuation,scattered);
           auto *ph = new photon;
           ph->position = rec2.p;
           ph->power = power;
           ph->direction = unit_ray.dir;
           photons.push_back(ph);
           return  emit_photons_medium(photons, world,
                    scattered, 
                    depth, power);
       }
    }
    const vec3 material_color = rec.mat_ptr->get_color();
    const vec3 unit_normal = unit_vector(rec.normal);
    const double s = rec.mat_ptr->get_specular_coefficient();
    const double d = rec.mat_ptr->get_diffuse_coefficient();
    roulette = random_double();
    double pr = std::max(material_color.e[0], std::max(material_color.e[1], material_color.e[2]));
    if (roulette >= pr * (s + d))
        return {0, 0, 0};
    rec.mat_ptr->scatter(r, rec, attenuation, scattered);
    if (roulette < pr * d) {
        return emit_photons_medium(photons, world, scattered, depth,  power * (1.0 / pr));
    }
    else {
        return emit_photons_medium(photons, world, scattered, depth, power);
    }
}

/*
 * Purpose: main function to be run which sets up the scene and produces the image
 * Inputs: none
 * Return value(s): none
 */
int main()
{
    std::ofstream MyFile("myimage.ppm");
    std::vector<photon*> photons;
    KDTree global_map{};
    hittable_list world;
    point3 light_position(0, 80, -20);
    auto material_lambertian_blue = make_shared<lambertian>(color(0, 0, 0.7));
    auto material_lambertian_red = make_shared<lambertian>(color(0.7, 0, 0));
    auto material_lambertian_bright = make_shared<lambertian>(color(0.9, 0.9, 0.9));
    auto material_lambertian_dark = make_shared<lambertian>(color(0.1, 0.1, 0.1));
    auto material_metal = make_shared<metal>(color(1, 1, 1));
    auto material_dielectric = make_shared<dielectric>(1.5);
    world.add(make_shared<aa_rect>(-1, 101, -100, 100, 50, material_lambertian_blue, 0)); // right
    world.add(make_shared<aa_rect>(-1, 101, -100, 100, -50, material_lambertian_red, 0)); // left
    world.add(make_shared<aa_rect>(-50, 50, -100, 100, 100, material_lambertian_bright, 1)); // top
    world.add(make_shared<aa_rect>(-50, 50, -100, 100, 0, material_lambertian_bright, 1)); // bottom
    world.add(make_shared<aa_rect>(-50, 50, 0, 100, -100, material_lambertian_bright, 2)); // back

    world.add(area_light);

    world.add(make_shared<sphere>(point3(-20, 20, -50), 20, material_metal));
    world.add(make_shared<sphere>(point3(20, 20, -10), 20, material_dielectric));
    world.add(make_shared<sphere>(point3(0, 5, -30), 5, material_lambertian_red));
 //   world.add(make_shared<sphere>(point3(20, 45, -30), 20, material_lambertian_blue));

    // csg addition
    auto csg_list = make_shared<hittable_list>();
    vec3 tvec(-20.0, 10.0, -10.0);  // translation vector - for positioning
    double scale = 10.0;            // scale multiplier
    // individual objects and materials
    auto green = make_shared<lambertian>(color(44.0/255.0, 201.0/255.0, 57.0/255.0));
    auto ycyl = make_shared<cylinder>(point3(0,-1.5,0)*scale + tvec, point3(0,1.5,0)*scale + tvec, 0.75*scale, green);
    auto xcyl = make_shared<cylinder>(point3(-1.5,0,0)*scale + tvec, point3(1.5,0,0)*scale + tvec, 0.75*scale, green);
    auto zcyl = make_shared<cylinder>(point3(0,0,-1.5)*scale + tvec, point3(0,0,1.5)*scale + tvec, 0.75*scale, green);
    auto red = make_shared<lambertian>(color(240.0/255.0, 96.0/255.0, 96.0/255.0));
    auto blue = make_shared<metal>(color(96.0/255.0, 185.0/255.0, 240.0/255.0));
    auto mybox = make_shared<aabox>(point3(-1,-1,-1)*scale + tvec, point3(1,1,1)*scale + tvec, red);
    auto mysphere = make_shared<sphere>(point3(0, 0, 0)*scale + tvec, 1.3*scale, blue);
    // csg union & intersection & difference
    auto cyl_cross = make_shared<csg_union>(make_shared<csg_union>(ycyl, xcyl), zcyl);
    auto box_sphere_itx = make_shared<csg_intersection>(mybox, mysphere);
    auto csg_obj = make_shared<csg_difference>(box_sphere_itx, cyl_cross);
    csg_list->add(csg_obj);
    world.add(make_shared<bvh_node>(csg_list));
    


    fprintf(stderr, "Emitting global map photons.\n");
    for (unsigned long i = 0; i < photon_emit_size; ++i) {
        // double random_angle = 2 * pi * random_double();
        // double t = 2 * acos(sqrt(1. - random_double()));
        // emit_photons(photons, world,
        //              ray(light_position, vec3(cos(random_angle) * sin(t), cos(t), sin(random_angle) * sin(t))),
        //              20, light_power);
        auto dir = random_in_hemisphere(vec3(0, -1, 0));
        emit_photons(photons, world,
                     ray(light_samples[i], dir),
                     20, light_power/photon_emit_size);

    }
    fprintf(stderr, "Emitted %d photons.\nBuilding kd-tree.\n", (int) photons.size());
    if (!photons.empty())
        global_map.build(&photons[0], photons.size());
    
    
    fprintf(stderr, "Emitting caustic map photons.\n");
    std::vector<photon*> caustic_photons;
    KDTree caustic_map{};
    for (unsigned long i = 0; i < num_caustic_photons; ++i) {
        hit_record rec;
        vec3 dir;
        ray r;
        char type = 'L';

        do {
            dir = random_in_hemisphere(vec3(0, -1, 0));
            r = ray(caustic_light_samples[i], dir);

            if (!world.hit(r, 0.0001, const_inf, rec)) {
                continue;
            }
            type = rec.mat_ptr->get_type();
        } while (type != 'D');// && type != 'M');

        emit_photons_caustic(caustic_photons, world,
                     r,
                     5, light_power/num_caustic_photons);

    }
    fprintf(stderr, "Emitted %d photons.\nBuilding kd-tree.\n", (int) caustic_photons.size());
    if (!caustic_photons.empty())
        caustic_map.build(&caustic_photons[0], caustic_photons.size());
    
    auto material_fog = make_shared<medium>(color(1.0, 1.0, 1.0));
    world.add(make_shared<halfsphere>(point3(0, 100, -20), radius_of_fogs, material_fog));
    medium_material_pointer = material_fog;
    fprintf(stderr, "Emitting participating media map photons.\n");
    std::vector<photon*> medium_photons;
    KDTree medium_map{};
    for (unsigned long i = 0; i < num_medium_photons; ++i) {
        hit_record rec;
        vec3 dir;
        ray r;
        char type = 'P';
        vec3 material_color;
        double pr;
        ray scattered;
        color attenuation;
        do {
            dir = random_in_hemisphere(vec3(0, -1, 0));
            r = ray(medium_light_samples[i], dir);

            if (!world.hit_without_medium(r, 0.0001, const_inf, rec)) {
                continue;
            }
            type = rec.mat_ptr->get_type();
            if (type !='P'){
                break;
            }
        } while (true);
        rec.mat_ptr->scatter(r, rec, attenuation, scattered);
        material_color = rec.mat_ptr->get_color();
        const vec3 unit_normal = unit_vector(rec.normal);
        const double d = rec.mat_ptr->get_diffuse_coefficient();
        double roulette = random_double();
        pr = std::max(material_color.e[0], std::max(material_color.e[1], material_color.e[2]));    
        if (roulette < pr * d) {
            emit_photons_medium(medium_photons, world, scattered ,5, (light_power/num_medium_photons) * (1.0 / pr));
        }
        else {
            emit_photons_medium(medium_photons, world, scattered ,5, (light_power/num_medium_photons));
        }
    }

    fprintf(stderr, "Emitted %d photons.\nBuilding kd-tree.\n", (int) medium_photons.size());
    if (!medium_photons.empty())
        medium_map.build(&medium_photons[0], medium_photons.size());
    

    camera cam(point3(0, 50, 99), point3(0, 50, 0), vec3(0, 1, 0), 90.0, aspect_ratio);
    sampling jittered_sampling = sampling(samples_per_pixel);
    MyFile << "P3\n" << image_width << ' ' << image_height << "\n255\n";
    auto start = chrono::high_resolution_clock::now();

    for (int j = image_height - 1; j >= 0; --j) { 
        std::cerr << "\rScanlines remaining: " << j << ' ' << std::flush;
        for (int i = 0; i < image_width; ++i) {
            color pixel_color = color(0, 0, 0);
            vector<vector<double>> sample_points = jittered_sampling.get_samples(i, j);
            for (auto & sample_point : sample_points) {
                ray r = cam.get_ray_perspective(sample_point[0] / image_width, sample_point[1] / image_height);
                pixel_color += ray_color_photon(global_map, world, r, 20, near_estimate_size);
                pixel_color += ray_color_caustic(caustic_map, world, r, caustic_near_estimate_size);
                r.dir = unit_vector(r.dir);
                pixel_color += ray_color_medium(medium_map, world, r, medium_near_estimate_size);
            }
            write_color(MyFile, pixel_color, samples_per_pixel);
        }
    }
    auto stop = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::milliseconds>(stop - start);
    cerr << "Time to render: " << duration.count() / 1000.0 << " s" << endl;
    
}
